xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";

import module namespace es = "http://marklogic.com/entity-services"
at "/MarkLogic/entity-services/entity-services.xqy";

declare option xdmp:mapping "false";

(:~
: Create Content Plugin
:
: @param $id          - the identifier returned by the collector
: @param $options     - a map containing options. Options are sent from Java
:
: @return - your transformed content
:)
declare function plugin:create-content(
  $id as xs:string,
  $options as map:map) as map:map
{
  let $doc := fn:doc($id)
  let $source := $doc
  return
  plugin:extract-instance-TestEntityNew($source)
};
  
(:~
: Creates a map:map instance from some source document.
: @param $source-node  A document or node that contains
:   data for populating a TestEntityNew
: @return A map:map instance with extracted data and
:   metadata about the instance.
:)
declare function plugin:extract-instance-TestEntityNew(
$source as node()?
) as map:map
{

  (: the original source documents :)
  let $attachments := $source
  let $source      :=
    if ($source/*:envelope and $source/node() instance of element()) then
      $source/*:envelope/*:instance/node()
    else if ($source/*:envelope) then
      $source/*:envelope/*:instance
    else if ($source/instance) then
      $source/instance
    else
      $source
  let $employee-i-d := xs:string($source/EmployeeID)
  let $employee-name := xs:string($source/employeeName)
  let $d-o-b := xs:string($source/DOB)
  let $account := xs:string($source/Account)
  let $manager-name := xs:string($source/ManagerName)

  (: return the in-memory instance :)
  (: using the XQuery 3.0 syntax... :)
  let $model := json:object()
  let $_ := (
    map:put($model, '$attachments', $attachments),
    map:put($model, '$type', 'TestEntityNew'),
    map:put($model, '$version', '1.0.0'),
    map:put($model, 'EmployeeID', $employee-i-d),
    map:put($model, 'employeeName', $employee-name),
    map:put($model, 'DOB', $d-o-b),
    map:put($model, 'Account', $account),
    map:put($model, 'ManagerName', $manager-name)
  )

  (: if you prefer the xquery 3.1 version with the => operator....
  https://www.w3.org/TR/xquery-31/#id-arrow-operator
  let $model :=
  json:object()
    =>map:with('$attachments', $attachments)
    =>map:with('$type', 'TestEntityNew')
    =>map:with('$version', '1.0.0')
      =>es:optional('EmployeeID', $employee-i-d )
    =>es:optional('employeeName', $employee-name)
    =>es:optional('DOB', $d-o-b)
    =>es:optional('Account', $account)
    =>es:optional('ManagerName', $manager-name)
  :)
  return $model
};

declare function plugin:make-reference-object(
$type as xs:string,
$ref as xs:string)
{
  let $o := json:object()
  let $_ := (
    map:put($o, '$type', $type),
    map:put($o, '$ref', $ref)
  )
  return
  $o
};