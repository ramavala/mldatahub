xquery version "1.0-ml";

import module namespace flow = "http://marklogic.com/data-hub/flow-lib" at "/data-hub/4/impl/flow-lib.xqy";

declare namespace alert = "http://marklogic.com/xdmp/alert";
declare namespace hub = "http://marklogic.com/data-hub";

declare option xdmp:mapping "false";

declare variable $alert:config-uri as xs:string external;
declare variable $alert:doc as node() external;
declare variable $alert:rule as element(alert:rule) external;
declare variable $alert:action as element(alert:action) external;

declare variable $ENTITY_NAME := $alert:action/alert:name/xs:string(.);
declare variable $FLOW_NAME := $alert:rule/alert:name/xs:string(.);
declare variable $FLOW_TYPE := "harmonize";

let $jobId := sem:uuid-string()
let $docUri := xdmp:node-uri($alert:doc)

let $options := map:new((
  map:entry('entity', $ENTITY_NAME),
  map:entry('dataFormat', 'xml'),
  map:entry('flow', $FLOW_NAME),
  map:entry('target-database', xdmp:database())
))

let $flow as element(hub:flow) := flow:get-flow($ENTITY_NAME, $FLOW_NAME, $FLOW_TYPE)

return
  if (fn:exists($flow)) then
    let $mainFunc := flow:get-main($flow/hub:main)
    return (
      flow:run-flow($jobId, $flow, $docUri, $options, $mainFunc),
      flow:run-writers($docUri)
    )
  else
    fn:error((),"RESTAPI-SRVEXERR", (404, "Not Found", "The requested flow was not found"))
