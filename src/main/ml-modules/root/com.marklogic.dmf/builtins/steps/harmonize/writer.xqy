xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";

import module namespace es = "http://marklogic.com/entity-services" at "/MarkLogic/entity-services/entity-services.xqy";
import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";

import module namespace temporal = "http://marklogic.com/xdmp/temporal" at "/MarkLogic/temporal.xqy";

declare option xdmp:mapping "false";

(:~
 : Writer Plugin
 :
 : @param $id       - the identifier returned by the collector
 : @param $envelope - the final envelope
 : @param $options  - a map containing options. Options are sent from Java
 :
 : @return - nothing
 :)
declare function plugin:write(
  $id as xs:string,
  $envelope as item(),
  $options as map:map) as empty-sequence()
{
  let $hid := xdmp:with-namespaces(("ns", $consts:NAMESPACE), $envelope/node()/es:headers/ns:hid/xs:string(.))
  let $uri := fn:concat($consts:NAMESPACE, "/", map:get($options, "entity"), "-", $hid, ".xml")
  let $collectorMode := map:get($options, 'collectorMode')
  let $collections :=
    for $c in fn:tokenize(map:get($options, "collections"), ',')
    where $c ne '@'
    return $c
  return
    plugin:document-insert(
      $uri,
      $envelope,
      xdmp:default-permissions(),
      ($consts:NAMESPACE || '/entity/' || fn:lower-case(map:get($options, "entity")), $consts:CANONICAL, $collections),
      $collectorMode
    )
};

(:~
 : Insert document into database in uni-temporal way
 :
 : @param uri The uri to store
 : @param envelope The DHF/es envelope. Typically a node
 : @param permissions The document permissions to be provided
 : @param collections Collections to provide
 : @param validTime document valid time
 : @param allowDuplicate Flag to allow duplicate content writing
 : @return
 :)
declare private function plugin:document-insert(
  $uri as xs:string,
  $envelope as item(),
  $permissions as item()*,
  $collections as item()*,
  $collectorMode as xs:string?
) as item()?
{
  if ($collectorMode eq 'refresh')
  then temporal:document-insert(
    $consts:BI-TEMPORAL,
    $uri,
    $envelope,
    $permissions,
    $collections
  )
  else
    let $previousChecksusm := fn:doc($uri)/node() ! xdmp:with-namespaces(("ns", $consts:NAMESPACE), ./es:headers/ns:checksum/xs:string(.))
    let $previousPid := fn:doc($uri)/node() ! xdmp:with-namespaces(("ns", $consts:NAMESPACE), ./es:headers/ns:pid/xs:string(.))
    let $currentChecksusm := (
      if (xdmp:node-kind($envelope) eq 'document') then $envelope/node() else $envelope
    ) ! xdmp:with-namespaces(("ns", $consts:NAMESPACE), ./es:headers/ns:checksum/xs:string(.))
    let $currentPid := (
      if (xdmp:node-kind($envelope) eq 'document') then $envelope/node() else $envelope
    ) ! xdmp:with-namespaces(("ns", $consts:NAMESPACE), ./es:headers/ns:pid/xs:string(.))
    return
      if ($currentChecksusm eq $previousChecksusm and $currentPid eq $previousPid)
      then (xdmp:log('Checksum and pid match. Not replacing ' || $uri))
      else (
        temporal:document-insert(
          $consts:BI-TEMPORAL,
          $uri,
          $envelope,
          $permissions,
          $collections
        )
      )
};
