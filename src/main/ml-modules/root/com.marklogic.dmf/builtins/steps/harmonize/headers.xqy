xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";

import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";
import module namespace headers = "http://abnamro.nl/data-hub/headers" at "/ext/data-hub/lib/headers-lib.xqy";
import module namespace validation = "http://abnamro.nl/data-hub/validation" at "/ext/data-hub/lib/validation-lib.xqy";
import module namespace functx = "http://www.functx.com" at "/MarkLogic/functx/functx-1.0-doc-2007-01.xqy";

declare option xdmp:mapping "false";

(:~
 : Create Headers Plugin
 :
 : @param $id      - the identifier returned by the collector
 : @param $content - the output of your content plugin
 : @param $options - a map containing options. Options are sent from Java
 :
 : @return - zero or more header nodes
 :)
declare function plugin:create-headers(
  $id as xs:string,
  $content as item()?,
  $options as map:map) as node()*
{
(: Validate the content aginst XSD :)
  let $err := validation:validate-xsd(map:get($content, "$ref"))
  let $pid :=  xdmp:md5(fn:string-join(($id, functx:sort(map:get($options, "prov:usage:uri"))), "::"))
  return (
    headers:generate-bit-headers(xdmp:md5(xdmp:quote($content)), map:get($options,'validDateTime')),
    if($options => map:contains("attributes")) then (
      headers:generate-sdm-headers(($options => map:get("attributes") => map:get("headers")) + map:entry("pid", $pid) + map:entry("job", map:get($options, "job")) + map:entry("onboardingVersion",map:get($options, "onboarding-version"))),
      headers:generate-sdm-errors($options => map:get("attributes") => map:get("errors"))
    ) else(),
    if (map:keys($err)) then
      xdmp:with-namespaces(("ns", $consts:NAMESPACE),
        element ns:errors {
          headers:generate-xsd-errors($err)
        }
      )
    else ()
  )
};
