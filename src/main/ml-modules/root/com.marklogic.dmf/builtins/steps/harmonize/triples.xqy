xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";

import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";
import module namespace provo = "http://abnamro.nl/data-hub/provo" at "/ext/data-hub/lib/provo-lib.xqy";
import module namespace triples = "http://abnamro.nl/data-hub/triples" at "/ext/data-hub/lib/triples-lib.xqy";

declare namespace sem = "http://marklogic.com/semantics";
declare option xdmp:mapping "false";

(:~
 : Create Triples Plugin
 :
 : @param $id      - the identifier returned by the collector
 : @param $content - the output of your content plugin
 : @param $headers - the output of your headers plugin
 : @param $options - a map containing options. Options are sent from Java
 :
 : @return - zero or more triples
 :)
declare function plugin:create-triples(
  $id as xs:string,
  $content as item()?,
  $headers as item()*,
  $options as map:map) as sem:triple*
{
  let $hid := xdmp:with-namespaces(("ns", $consts:NAMESPACE), <headers>{$headers}</headers>/ns:hid/xs:string(.))
  let $entity := sem:iri(fn:concat($consts:NAMESPACE, "/", fn:lower-case(map:get($options, "entity")), "-", $hid))
  let $agent := triples:generate-iri("system",  map:get($options, "source"))
  let $activity := triples:generate-iri("activity", map:get($options, "flow"))

  return (
    provo:entity(
      $entity,
      (triples:generate-prov-type(map:get($options, "entity"))),
      map:new((
        map:entry("prov:hadPrimarySource", sem:iri($id)),
        map:entry("prov:wasAttributedTo", $agent)
      ))
    ),
    provo:wasGeneratedBy(
      $entity,
      $activity,
      fn:current-dateTime()
    ),
    map:get($options, "prov:usage:uri") ! provo:used(
      $activity, sem:iri(.)
    )
  )
};
