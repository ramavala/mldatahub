xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";
import module namespace collector = "http://abnamro.nl/data-hub/collector" at "/ext/data-hub/lib/collector-lib.xqy";
import module namespace mapping = "http://abnamro.nl/data-hub/mapping" at "/ext/data-hub/lib/mapping-sdm.xqy";
import module namespace utils = "http://abnamro.nl/data-hub/utils" at "/ext/data-hub/lib/utils-lib.xqy";
import module namespace model = "http://abnamro.nl/data-hub/model" at "/ext/data-hub/lib/model-lib.xqy";

declare option xdmp:mapping "false";

(:~
 : Collect IDs plugin
 :
 : @param $options - a map containing options. Options are sent from Java
 :
 : @return - a sequence of ids or uris
 :)
declare function plugin:collect(
  $options as map:map) as xs:string*
{
  collector:get-uris($options),

  (: Warming up the mapping cache :)
  utils:ignore(mapping:get-sdm(map:get($options, "entity"), map:get($options, "flow"), "harmonize" , fn:true())),
  utils:ignore(model:refresh-model-cache(map:get($options, "entity"), fn:true()))
};

