xquery version "1.0-ml";

import module namespace test = 'http://marklogic.com/roxy/test-helper' at '/test/test-helper.xqy';
import module namespace SUT = "http://marklogic.com/data-hub/plugins" at "/com.marklogic.dmf/builtins/steps/status/collector.xqy";

declare function local:get-options ($collectorMode, $table) {
    map:new((
        map:entry('collectorMode', $collectorMode),
        map:entry('jobId', 'unit-test-com-marklogic-dmf'),
        map:entry('source', 'ACBS'),
        map:entry('entity', 'CreditAgreement'),
        map:entry('filterRoot', $table),
        map:entry('jobId', 'unit-test-com-marklogic-dmf'),
        map:entry('collection', '/injob/unit-test-com-marklogic-dmf')
    ))
};
let $res := SUT:collect(local:get-options('latest', 'JCAMS'))
return (
    test:assert-equal(fn:count($res), 1),
    (: check that expected uri was found :)
    test:assert-equal($res[1], 'http://nl.abnamro.com/cre/acbs/jcams-123-unit-test-456.xml')
)
,
let $res := SUT:collect(local:get-options('latest', 'JCAMI'))
return ( test:assert-equal(fn:count($res), 3))
,
let $res := SUT:collect(local:get-options('latestNoDeletes', 'JCAMI'))
return ( test:assert-equal(fn:count($res), 2))
,
let $res := SUT:collect(local:get-options('latestNoDeletes', 'JCAMS'))
return ( test:assert-equal(fn:count($res), 0))
,
let $res := SUT:collect(local:get-options('latestDeletes', 'JCAMI'))
return ( test:assert-equal(fn:count($res), 1))
,
let $res := SUT:collect(local:get-options('latestDeletes', 'JCAMS'))
return ( test:assert-equal(fn:count($res), 1))
,
let $res := SUT:collect(local:get-options('removalsByJob', 'JCAMI'))
return ( test:assert-equal(fn:count($res), 1))
,
(: Should not pick up the job removed by a different collection :)
let $res := SUT:collect(local:get-options('removalsByJob', 'JCAMS'))
return ( test:assert-equal(fn:count($res), 0))
