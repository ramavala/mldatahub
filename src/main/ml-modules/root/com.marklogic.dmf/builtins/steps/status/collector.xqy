xquery version "1.0-ml";
module namespace plugin = "http://marklogic.com/data-hub/plugins";
import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";
declare option xdmp:mapping "false";
(:~
 : Status collector - returns URIS matching a named status
 : -collectorMode {STATUS}
 :  OPTIONS
 :  - STATUS: one of latest, replaced, deletes
 :  All are scoped by table, source and raw data
 :  - latest: all records in the latest collection 
 :  - latestNoDeletes: all records in the latest collection except deletes
 :  - latestDeletes: all deletes in the latest collection
 :  - removalsByJob: all records remove from latest by this job
 :
 : @param $options - a map containing options. Options are sent from Java
 :
 : @return - a sequence of ids or uris
 :)
declare function plugin:collect(
  $options as map:map
) as xs:string*
{
  let $cMode := map:get($options, "collectorMode") 
  let $jobId := map:get($options, "jobId")
  let $source := fn:lower-case(map:get($options, "source"))
  let $entity := fn:lower-case(map:get($options, "entity"))
  let $filterRoot := map:get($options, "filterRoot")
  let $extraCollections := map:get($options, "collection")
    
  (: gather common collections and queries :)
  let $sourceCollection := fn:concat($consts:NAMESPACE, "/source/",$source)
  let $rawCollection := fn:concat($consts:NAMESPACE, "/raw")
  let $entityCollection := fn:concat($consts:NAMESPACE, "/entity/" || $entity)
  let $collections := (
    fn:tokenize($extraCollections,','),
    $entityCollection, 
    $sourceCollection,
    $rawCollection
  )
  let $tableQuery :=  cts:element-query(
    fn:QName(fn:concat($consts:NAMESPACE,"/", $source), $filterRoot),cts:and-query(())
  )
  let $deletesQuery :=  cts:element-value-query(
    fn:QName(fn:concat($consts:NAMESPACE,"/", $source), 'ACTION'), 'D'
  )
  return cts:uris((), (), cts:and-query((
      $collections ! cts:collection-query(.), $tableQuery,
      switch ($cMode)
      case 'removalsByJob' return (
        cts:collection-query(( "/removed-by/" || $jobId)),
        cts:not-query(cts:collection-query("latest"))
      )
      case 'latest' return (
        cts:collection-query(("latest"))
      )
      case 'latestNoDeletes' return (
        cts:not-query($deletesQuery),
        cts:collection-query(("latest"))
      )
      case 'latestDeletes' return (
        $deletesQuery,
        cts:collection-query("latest")
      )
      default return ()
    )))
};
