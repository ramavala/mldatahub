xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";

import module namespace temporal = "http://marklogic.com/xdmp/temporal" at "/MarkLogic/temporal.xqy";
import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";

declare option xdmp:mapping "false";

(:~
 : Writer Plugin
 :
 : @param $id       - the identifier returned by the collector
 : @param $envelope - the final envelope
 : @param $options  - a map containing options. Options are sent from Java
 :
 : @return - nothing
 :)
declare function plugin:write(
  $id as xs:string,
  $headers as map:map,
  $options as map:map) as empty-sequence()
{
  temporal:document-delete($consts:BI-TEMPORAL, $id)
};
