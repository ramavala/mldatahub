xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";
import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";

declare option xdmp:mapping "false";

(:~
 : Collect IDs plugin
 :
 : @param $options - a map containing options. Options are sent from Java
 :
 : @return - a sequence of ids or uris
 :)
declare function plugin:collect(
  $options as map:map) as xs:string*
{
  cts:uris((), (),
    cts:and-query((
      cts:not-query(cts:element-value-query(fn:QName($consts:NAMESPACE,"job"), map:get($options, "job"), "exact")),
      cts:collection-query("latest"),
      cts:collection-query(fn:concat($consts:NAMESPACE, "/canonical")),
      cts:collection-query(fn:concat($consts:NAMESPACE, "/entity/", fn:lower-case(map:get($options, "entity"))))
    ))
  )
};
