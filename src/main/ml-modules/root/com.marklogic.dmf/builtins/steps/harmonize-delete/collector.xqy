xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";
import module namespace context = "http://abnamro.nl/data-hub/context" at "/ext/data-hub/lib/context-lib.xqy";
import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";

declare option xdmp:mapping "false";

(:~
 : Collect IDs plugin
 :
 : @param $options - a map containing options. Options are sent from Java
 :
 : @return - a sequence of ids or uris
 :)
declare function plugin:collect(
  $options as map:map) as xs:string*
{
  cts:uris((), (),
    cts:and-query((
      map:get($options, "filterJob") ! cts:element-value-query(fn:QName($consts:NAMESPACE,"job"), ., "exact"),
      context:collector-query(map:get($options, "entity"), map:get($options, "flow"))
    ))
  )
};
