xquery version "1.0-ml";

module namespace plugin = "http://marklogic.com/data-hub/plugins";

import module namespace temporal = "http://marklogic.com/xdmp/temporal" at "/MarkLogic/temporal.xqy";
import module namespace consts = "http://abnamro.nl/data-hub/consts" at "/ext/data-hub/consts/config.xqy";

declare option xdmp:mapping "false";

(:~
 : Writer Plugin
 :
 : @param $id       - the identifier returned by the collector
 : @param $envelope - the final envelope
 : @param $options  - a map containing options. Options are sent from Java
 :
 : @return - nothing
 :)
declare function plugin:write(
  $id as xs:string,
  $headers as map:map,
  $options as map:map) as empty-sequence()
{
  let $uri := fn:concat($consts:NAMESPACE, "/", $options => map:get("entity"), "-", $headers => map:get("hid"), ".xml")
  return try {
     temporal:document-delete($consts:BI-TEMPORAL, $uri),
     xdmp:document-remove-collections($id,'latest')
  } catch($e) {
    let $errorCode := $e//*:code/xs:string(.)
    return 
      if ($errorCode eq 'XDMP-DOCNOTFOUND')
      then (
        xdmp:log(("CRE-EVENT-DELETE-NOT-POSSIBLE " || $uri)),
        xdmp:document-remove-collections($id,'latest')
      ) 
      else xdmp:rethrow()
  }    
};