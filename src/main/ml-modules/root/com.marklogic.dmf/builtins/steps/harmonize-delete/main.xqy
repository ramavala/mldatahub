xquery version "1.0-ml";

(: Your plugin must be in this namespace for the DHF to recognize it:)
module namespace plugin = "http://marklogic.com/data-hub/plugins";
(:
 : This module exposes helper functions to make your life easier
 : See documentation at:
 : https://github.com/marklogic/marklogic-data-hub/wiki/dhf-lib
 :)

import module namespace mapping = "http://abnamro.nl/data-hub/mapping" at "/ext/data-hub/lib/mapping-sdm.xqy";

(: include the writer module which persists your envelope into MarkLogic :)
import module namespace writer = "http://marklogic.com/data-hub/plugins" at "writer.xqy";

import module namespace dhf = "http://marklogic.com/dhf" at "/data-hub/4/dhf.xqy";


declare namespace es = "http://marklogic.com/entity-services";

declare option xdmp:mapping "false";

(:~
 : Plugin Entry point
 :
 : @param $id          - the identifier returned by the collector
 : @param $options     - a map containing options. Options are sent from Java
 :
 :)
declare function plugin:main(
  $id as xs:string,
  $options as map:map)
{
  let $doc := fn:doc($id)
  let $source :=
    if ($doc/es:envelope) then
      $doc/es:envelope/es:instance/node()
    else if ($doc/instance) then
      $doc/instance
    else
      $doc

  let $map := mapping:extract-harmonize-headers(map:get($options, "entity"), map:get($options, "flow"),$source)

  return
    dhf:run-writer(xdmp:function(xs:QName("writer:write")),$id , map:get($map, "headers"), $options)

};
