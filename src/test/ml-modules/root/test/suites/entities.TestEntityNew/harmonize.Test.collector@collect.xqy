xquery version '1.0-ml';

import module namespace test = 'http://marklogic.com/test' at '/test/test-helper.xqy';

test:assert-true(fn:count(cts:uris((), (), cts:collection-query("TestEntityNew"))) = 4),
test:log("Count is correct....")